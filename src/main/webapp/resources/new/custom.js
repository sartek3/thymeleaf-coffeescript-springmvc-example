(function() {

  document.addEventListener("DOMContentLoaded", function() {
    $("#example1").click(function() {
      $('ul.nav li').removeClass("active");
      $.get("example1", function(data) {
        return $("#windowContent").html(data);
      });
      return $(this).parent().addClass("active");
    });
    $("#example2").click(function() {
      $('ul.nav li').removeClass("active");
      $.get("example2", function(data) {
        return $("#windowContent").html(data);
      });
      return $(this).parent().addClass("active");
    });
    $("#example3").click(function() {
      $('ul.nav li').removeClass("active");
      $.get("example3", function(data) {
        return $("#windowContent").html(data);
      });
      return $(this).parent().addClass("active");
    });
    return $("#example4").click(function() {
      $('ul.nav li').removeClass("active");
      $.get("example4", function(data) {
        return $("#windowContent").html(data);
      });
      return $(this).parent().addClass("active");
    });
  });

}).call(this);
